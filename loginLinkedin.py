#só utilizaremos essas duas bibliotecas
import requests
from bs4 import BeautifulSoup
from getSenhas import getSenha
import json
"""principios básicos:
 - Todo formulário de login tem a Tag HTML <form>, e é isso o que iremos buscar em todos os sites
 - Dentro desses <form> existem campos de Tag <input> que armazena <value> valores que iremos utilizar
 - Todo login é feito a partir de um "POST" dentro de uma API um dentro do próprio Site.
"""
def login():
    #instanciando sessão do requests
    session = requests.session()
    """
    A partir de agora, todas nossas requisições serão feitas por session
    """

    #fazendo requisição do código fonte da página
    content = session.get('https://www.linkedin.com/uas/login')
    #parseando a nossa requisição
    #Precisamos transformar em text para podermos parsear
    #aqui utilizarei o 'lxml' para parsear
    tree = BeautifulSoup(content.text, 'lxml')

    #vamos printar para ver o que estamos recebendo
    # print(tree)

    '''agora vamos procurar pelo <form>
    utilizaremos o find
    encontramos apenas 1, e será o único que precisaremos
    como pode ser nortado, há um 'method'='POST' dentro desse form, e é o método que utilizaremos para fazer o login
    para deixar mais sólido nossos inputs, podemos especificar a classe ou o nome/id dele
    '''
    #tree.find('tag', {'class, name ou id', 'tag da class'}).findAll('input', {'type': 'hidden'})
    '''vamos dar um findAll agora em todos esses inputs'''

    inputs = tree.find('form', {"class":"login__form"}).findAll('input', {'type': 'hidden'})


    '''
    O que precisamos fazer agora, é buscar todos os values que são necessários para utilizar como parâmetro no nosso post
    Normalmente todos esse parametros estão nos inputs do site.
    Tendo todos os inputs, precisamos transformar ele em um dicionário, para passar como parâmetro no POST!
    vamos fazer um for nos inputs
    '''
    data = {input.get('name'): input.get('value') for input in inputs}

    ''' vamos ver o que isso trouxe
    {'csrfToken': 'ajax:1470426137562865630', 'ac': '0', 'sIdString': '63b5cfe7-2c29-4c1f-82a6-f0b02a90736b',
    'controlId': 'd_checkpoint_lg_consumerLogin-login_submit_button', 'parentPageKey': 'd_checkpoint_lg_consumerLogin',
    'pageInstance': 'urn:li:page:d_checkpoint_lg_consumerLogin;+2pn1R3jR++uAlJhNVYdHg==', 'trk': '', 'session_redirect': ''
    , 'loginCsrfParam': '0828ad4f-4ec1-4cf6-8c7c-5c9a0d2bbad9', '_d': 'd'}

    temos todos os campos necessários, agora precisamos apenas colocar nossos email e senha em data
    antes dar um verificada no nome do campo de senha e email
    para ter certeza que está certo, podemos fazer um login de teste
    '''
    "Errei no parametro do email, erro simples que pode acabar com tudo hehe.. relevem minha burrice"

    data['session_key'] = 'fer_bergamin@hotmail.com'
    data['session_password'] = getSenha(2)

    linkPost = 'https://www.linkedin.com'+tree.find('form', {"class":"login__form"})['action']
    print(linkPost)

    '''
    Agora que temos todos os campos necessários, podemos tentar fazer o post
    Lembrando, sempre com o session, pois ele armazena seu login!
    Detalhe importante, devemos sempre verificar qual o link de POST
    Podemos Capturar esse link dando um find no FORM
    /checkpoint/lg/login-submit <- esse é o nosso endereço de post
    '''

    response = session.post(linkPost , data = data)
    'vamos printar nosso reponse'
    'aparentemente, deu tudo certo. vamos tentar minerar alguma páginas que exija login para testar'
    'vou disponibilizar esse código no GitLab'
    'só digitar !gitlablogins no chat'
    print(response.status_code)
    requestInitialPage(session=session)

#criando função para minerar a página
def requestInitialPage(session):
    # chamando a função login que me faz login e me retorna a session
    link = 'roberto-jos%C3%A9-ferreira-721687119'
    name = link.split('-')[0]
    pageTree = session.get('https://www.linkedin.com/in/'+link+'/')
    pageContent = BeautifulSoup(pageTree.text, 'lxml')

    '''vamos ver o que isso nos retorna
    provavelmente vai ser apenas códigos, pois o linkedin é um site "criptografado" e acaba sendo difícil pegar qualquer
    tipo de informação de forma limpa
    '''

    '''
        Ele nos retornou o feed, porém "criptografado"...
        Caso não estivessemos logado, ele iria nos retornar a página de sessão
        Vamos tentar pegar infos de algum perfil
        O linkedin nos bloqueou hehe.. Acontece... Vamos tentar algum perfil que não tenha acentos no link
        Acredito que seja algum problema com a session
        Algo de errado ocorreu no nosso post, precisamos verificar, mesmo com o response 200
        Agora sim estamos dentro... haha
        Se perceberem, a maioria dos dados vem em uma espécie de JSON... Podemos separar alguns trechos e parsear
        com a biblioteca JSON! Também é perceptível muitos "erros" de encode dentro do content
        Vamos tentar dar um find nessa tag e pegar o conteúdo para parsear
        Se não me engano, a tag code não é parseada no lxml, mas vamos tentar
        aparentemente o conteúdo que queremos está fora de qualquer tag... O que torna isso um pouco mais difícil :c
        aparentemente o id é aleatório, isso deixa muitoooo hard
        exatamente.. huahauhuahaha
        Esse site exige gambiarras
        Finalmente temos nosso conteúdo haha
        Agora podemos tentar parsear ele com json
    '''
    contentPerfil = pageContent.findAll('code')
    for codeTags in contentPerfil:
        if '"firstName":"'+name.capitalize() in str(codeTags):
            contentParsedJson = json.loads(codeTags.text)
            break
    """
        aparentemente deu certo. vamos tentar buscar algum elemento... Percebe-se que a tag principal é a data!
        fino!!! deu certo hehe
        agora podemos manipular fácilmente, da maneira que quisermos esses dados! Na base da lei, é claro ^.^
        Vamos tentar pegar os dados principais do perfil, como nome e localização... Vamos lá
        Só preciso descobrir dentro de onde essas informações estão auahuaha
    """
    for each in contentParsedJson['included']:
        if 'summary' in str(each):
            perfilInfos = each

    print(perfilInfos)
    """Aqui temos todas as informações básicas do perfil, só resta limpar e jogar em um banco ou em um csv"""
    nome = perfilInfos['firstName']+" "+perfilInfos['lastName']
    localização = perfilInfos['locationName']
    localDeTrabalho = perfilInfos['industryName']

    print("Perfil de {name}, trabalha com {trabalho} e mora em {live}".format(name = nome, trabalho = localDeTrabalho, live= localização))

login()
