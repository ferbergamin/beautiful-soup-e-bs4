import csv
import base64

def getSenha(position):
    arquivo = open('senhas.csv', 'r')
    for line in csv.reader(arquivo, delimiter=','):
        senha = line[position].encode()
        senhaD = base64.b64decode(senha)
        senhaDecoded = senhaD.decode()
        return senhaDecoded
